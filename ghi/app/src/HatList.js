import React, { useState } from 'react';

function HatList(props) {
    // const { hats } = props
    const [hats, setHats] = useState(props.hats);
    const deleteHat = (id) => {
        fetch(`http://localhost:8000/api/hats/${id}/`, {
          method: "DELETE",
        })
          .then(response => {
            if (!response.ok) {
              throw new Error("Failed to delete hat");
            }
            // Remove the hat from the list
            const updatedHats = hats.filter(hat => hat.id !== id);
            // props.setHats(updatedHats);

            setHats(updatedHats);
          })
          .catch(error => {
            console.error(error);
          });
      }
    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Fabric</th>
                    <th>Style name</th>
                    <th>Color</th>
                    <th>Picture</th>
                    <th>Location</th>
                </tr>
            </thead>
            <tbody>
                {props.hats && props.hats.map(hat => {
                    console.log(props)
                    return (
                    <tr key={ hat.id }>
                        <td>{ hat.fabric }</td>
                        <td>{ hat.style_name }</td>
                        <td>{ hat.color }</td>
                        <td>
                            <img src={ hat.picture_url } width="180" height="140"/>
                        </td>
                        <td>{ hat.location }</td>
                        <td>
                            <button onClick={() => deleteHat(hat.id)}>Delete</button>
                        </td>
                    </tr>
                    );
                })}
             </tbody>
        </table>
    );
}

export default HatList;
