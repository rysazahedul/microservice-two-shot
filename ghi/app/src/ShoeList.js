import React, { useState } from "react";


function ShoeList(props){
    const [shoes, setShoes] = useState(props.shoes);
    const deleteShoe = (id) => {
        fetch(`http://localhost:8080/api/shoes/${id}`, {
          method: "DELETE",
        })
          .then(response => {
            if (!response.ok) {
              throw new Error("Failed to delete shoe");
            }

            // Remove the shoe from the list
            const updatedShoes = shoes.filter(shoe => shoe.id !== id);

            setShoes(updatedShoes);
          })
          .catch(error => {
            console.error(error);
          });
      }
      console.log(shoes)

    return (
        <table className="table table=striped">
            <thead>
                <tr>
                    <th>Shoe</th>
                    <th>Manufacturer</th>
                    <th>Color</th>
                    <th>URL</th>
                    <th>Bin</th>
                </tr>
            </thead>
            <tbody>
                {shoes?.map(shoe => {
                    return(
                        <tr key={shoe.id}>
                            <td>{ shoe.model_name }</td>
                            <td>{ shoe.manufacturer }</td>
                            <td>{ shoe.color }</td>
                            <td>
                                <img src={ shoe.url } width="180" height="140"/>
                            </td>
                            <td>{ shoe.bin }</td>
                            <td>
                                <button onClick={() => deleteShoe(shoe.id)}>Delete</button>
                            </td>
                        </tr>
                    );
                })}

            </tbody>
        </table>
    )
}

export default ShoeList
