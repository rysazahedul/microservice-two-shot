import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(
//   <React.StrictMode>
//     <App />
//   </React.StrictMode>
// );

async function loadData() {
  const response = await fetch('http://localhost:8090/api/hats/')
  const shoeResponse = await fetch('http://localhost:8080/api/shoes/');
  if (response.ok && shoeResponse.ok)  {
    const data = await response.json();
    const shoeData = await shoeResponse.json();
    console.log(data, shoeData)
    root.render(
      <React.StrictMode>
        <App hats={data.hats} shoes={shoeData.shoes} />
      </React.StrictMode>
    );
  } else {
    console.error(response);
  }
}
loadData();

// async function loadShoes() {
//   const response = await fetch('http://localhost:8080/api/shoes/');
//   if (response.ok) {
//     const data = await response.json();
//     root.render(
//       <React.StrictMode>
//         <App shoes={data.shoes} />
//       </React.StrictMode>
//     );
//   } else {
//     console.error(response);
//   }
// }
// //loadShoes();
