import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatList from './HatList';
import React from 'react';
import ShoeList from './ShoeList';

function App(props) {
  // if (props.shoes === undefined) {
  //   return null;
  // }
  // console.log(props)
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats" element={<HatList hats={props.hats} />} />
          <Route path="shoes">
            <Route path='' element={<ShoeList shoes={props.shoes} />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
    // <ShoeList shoes={props.shoes} />
  );
}

export default App;
