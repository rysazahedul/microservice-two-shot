# Wardrobify

Team:

* Rysa Zahedul Hats
* Stesha Carle Shoes


## Design
We will use Bootstrap/CSS to style our application.

## Shoes microservice

There will be a Shoe model.  For view functions, there will be list_shoes, shoe_detail, create a hatVO that will be used to communicate with the wardrobe microservice

## Hats microservice

The Hat model will interact with the API call view functions to list hats, show details, and create/update/delete the details.
